<?php
add_action('admin_menu','wp_apis_register_menu');
function wp_apis_register_menu(){
    add_menu_page(
        'پلاگین سفارشی',
        'پلاگین سفارشی',
        'manage_options',
        'wp_apis_admin',
        'wp_apis_main_menu_handler'
    );
    add_submenu_page(
        'wp_apis_admin',
        'کاربران',
        'کاربران',
        'manage_options',
        'wp_apis_users',
        'wp_apis_users_page'
    );
    add_submenu_page(
        'wp_apis_admin',
        'تنظیمات',
        'تنظیمات',
        'manage_options',
        'wp_apis',
        'wp_apis_general_page'
    );
}
function wp_apis_main_menu_handler(){
    global $wpdb;
    $action = $_GET['action'];

//*************if for ---delete---- item to table ,database***************** */

    if($action == "delete")
    {
        $item = intval($_GET['item']);
        if($item > 0)
        {
            $wpdb->delete($wpdb->prefix.'sample',['ID'=> $item]);
        }
        return;
    }

//************* *********** condition for [[  adding  ]] item to table ,database***************** */

    if($action == "add")
    {
        if(isset($_POST['saveData'])){
            $wpdb->insert($wpdb->prefix . 'sample', [
                'fname' => $_POST['firstName'],
                'lname' => $_POST['lastName'],
                'mobile' => $_POST['num-mobile'],] );
        }
        include WP_APIS_TPL.'tpl-admin/menu/add.php';
        return;
    }
    else
    {
        $samples = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}sample");
        include WP_APIS_TPL.'tpl-admin/menu/tpl-admin-menu.php';
    }

 //*************if for ----updating & edit---- item to table ,database***************** */

    if($action == "update")
    {
        $item = intval($_GET['item']);
        if(($item > 0) && (isset($_POST['updateData'])))
        {
            $wpdb->update($wpdb->prefix . 'sample', 
            [
                'fname' => $_POST['firstName'],
                'lname' => $_POST['lastName'],
                'mobile' => $_POST['num-mobile'],
            ] ,
            ['ID'=>$item]);
        }
        $show_sample = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}sample WHERE ID=$item");
        include WP_APIS_TPL.'/tpl-admin/menu/update.php';
    }
}

/****************** a function for adding sub menu general in dashboard admin********************* */

function wp_apis_general_page(){

    if(isset($_POST['saveSettings']))
    {
        if(isset($_POST['is_plugin_active'])){
            update_option('wp_apis_is_active',1);
        }
        else{
            delete_option('wp_apis_is_active');
        }
   }
    $current_plugin_status = get_option('wp_apis_is_active',0);
    include WP_APIS_TPL.'tpl-admin/menu/general.php';
}
/* *************** a function for submenu users ***************************** 
****************** add , edit & delete item from databases defult wordpress */
 
function wp_apis_users_page()
{
    global $wpdb;
    
    if(isset($_GET['action']) && $_GET['action']== 'addUser')
    {   

        if(isset($_POST['saveDataUser'])  )
        {
            if($_POST['firstNameUser'] != [] && $_POST['emailUser'] != [] && $_POST['passUser'] != []){
                $new_user_result = wp_insert_user
            (
                [
                    'user_login ' => $_POST ['firstNameUser'],
                    'user_email ' => $_POST ['emailUser'    ],
                    'user_pass  ' => $_POST ['passUser'     ],
                ]
            );
            }
        }
        include WP_APIS_TPL.'tpl-admin/menu/users/add-vip.php';
        return;
    }
/* ******************** condition for [ edit & update  ]  data of the user **********************/
    
    if(isset($_GET['action']) && $_GET['action']== 'edit')
    {
        $userID = intval($_GET['id']);
        if(isset($_POST ['saveUserInfo'])){
            $mobile    = $_POST['mobile'  ];
            $wallet    = $_POST['wallet'  ];
            $emailUser = $_POST['emailUser'];
            if(!empty($mobile))
            {
                update_user_meta($userID , 'mobile' ,$mobile);
            }
            if(!empty($wallet))
            {
                update_user_meta($userID , 'wallet' ,$wallet);
            }
            if(!empty($emailUser))
            {
                wp_update_user
                ([
                    'ID'         => $userID ,
                    'user_email' => $emailUser
                ]);
            }
        }
        $mobile = get_user_meta($userID,'mobile',true);
        $wallet = get_user_meta($userID,'wallet',true);
        include WP_APIS_TPL.'tpl-admin/menu/users/edit-vip.php';
        return;
    }
/* ******************** condition for [[  delete  ]]  data of the user ***********************/

    if(isset($_GET['action']) && $_GET['action'] = 'removeUser')
    {
        $userID = intval($_GET['id']);
        wp_delete_user( $userID, 1 );
        //delete_user_meta( $userID, 'mobile' );
        //delete_user_meta( $userID, 'wallet' );
    }
    if(isset($_GET['action']) && $_GET['action'] = 'removeMobile' )
    {
        $userID = intval($_GET['id']);
        delete_user_meta( $userID,'mobile');
    }
    if(isset($_GET['action']) && $_GET['action'] = 'removeWallet' )
    {
        $userID = intval($_GET['id']);
        delete_user_meta( $userID,'wallet');
    }
    
    $users=$wpdb->get_results("SELECT ID,user_email,display_name FROM {$wpdb->users}");
    include WP_APIS_TPL.'tpl-admin/menu/users/users-vip.php';
}
?>

