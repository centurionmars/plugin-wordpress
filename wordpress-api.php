<?php
/*
Plugin Name: WP Apis
Plugin URI: https://cyclestart.ir/
Description: A plugin for wordpress .
Author: centurion mars
Author URI: https://cyclestart.ir/
Text Domain: apis 
Domain Path: /languages/
Version: 1.0.10
*/
define('WP_APIS_DIR',plugin_dir_path(__FILE__));
define('WP_APIS_URL',plugin_dir_url(__FILE__));
define('WP_APIS_INC',WP_APIS_DIR.'/inc/');
define('WP_APIS_TPL',WP_APIS_DIR.'/tpl/');

register_activation_hook(__FILE__,'wp_apis_plugin_activation');
register_deactivation_hook(__FILE__,'wp_apis_plugin_deactivation');

function wp_apis_plugin_activation(){
    add_role( 
        'Shop_admin',
        'مدیر فروشگاه',
        [
            'read' => true,
            'edit_post' => true,
            'remove_product' => true
        ]
     );
     $role=get_role('Shop_admin');
     $role -> add_cap('remove_product'
    );
}
function wp_apis_plugin_deactivation(){
}
if(is_admin())
{
    include WP_APIS_INC.'admin/menus.php';
    include WP_APIS_INC.'admin/metaboxes.php';
}
include WP_APIS_INC.'ajax.php';

function wpapis_register_style()
{
    wp_register_style('wpapis-main-style' , WP_APIS_URL.'assests/css/main.css');
    wp_enqueue_style('wpapis-main-style');
}
//add_action('wp_enqueue_scripts' ,'wpapis_register_style');
add_action('admin_enqueue_scripts' ,'wpapis_register_style');