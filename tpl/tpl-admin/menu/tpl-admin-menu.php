<div class="wrap">
  <h1>لیست اطلاعات</h1>
  <a href="
  <?php echo add_query_arg(['action' => 'add']); ?>
  ">ثبت داده جدید</a>
      <table class="widefat tabShadow">
          <thead>
            <tr>
              <th>آیدی</th>
              <th>نام</th>
              <th>نام خانوادگی</th>
              <th>موبایل</th>
              <th>عملیات</th>
            </tr>
          </thead>
          <tbody>

          <?php foreach($samples as $sample): ?>
            <tr>
              <td><?php echo $sample->ID; ?></td>
              <td><?php echo $sample->fname; ?></td>
              <td><?php echo $sample->lname; ?></td>
              <td><?php echo $sample->mobile; ?></td>
              <td>
                <a href="<?php echo add_query_arg(['action'=>'delete','item'=> $sample->ID]) ?>"><span class="dashicons dashicons-trash"></span> </a>
                <a href="<?php echo add_query_arg(['action'=>'update','item'=> $sample->ID]) ?>" class="update"><span class="dashicons dashicons-edit-large"></span></a>
              </td>
            </tr>
            <?php endforeach; ?>

          </tbody>
      </table>
</div>
