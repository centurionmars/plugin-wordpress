<div class="wrap">
    <h1>کاربران ویژه</h1>
    <a title="ثبت کاربر ویژه" href="
    <?php echo add_query_arg(['action' => 'addUser']); ?>
    " style="text-decoration: none;"><span class="dashicons dashicons-plus"></span></a>
    <button id="sendAjaxRequest" class="button">send Ajax Request</button>
    <table class="widefat tabShadow">
        <thead>
            <tr>
                <th>شناسه</th>
                <th>نام کامل</th>
                <th>ایمیل</th>
                <th>شماره همراه</th>
                <th>کیف پول</th>
                <td>عملیات</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach($users as $user): ?>
            <?php 
            $userWallet = get_user_meta($user->ID,'wallet',true);
            $userWallet = empty($userWallet) ? 0 : $userWallet;
            ?>
                <tr>
                    <td><?php echo $user->ID; ?></td>
                    <td><?php echo $user->display_name; ?></td>
                    <td><?php echo $user->user_email; ?></td>
                    <td>
                        <a title="حذف شماره موبایل" href="<?php echo add_query_arg(['action' => 'removeMobile' , 'id' =>$user ->ID ]); ?>">
                        <span class="dashicons dashicons-trash"></span></a>
                        <?php echo get_user_meta($user->ID,'mobile',true);?> 
                    </td>
                    <td>
                        <a title="حذف کیف پول" href="<?php echo add_query_arg(['action' => 'removeWallet' , 'id' =>$user ->ID ]); ?>">
                        <span class="dashicons dashicons-trash"></span></a>
                        <?php echo number_format($userWallet) . ' تومان ' ?>
                    </td>
                    <td>
                        <a title="ویرایش کیف پول و شماره موبایل" href="<?php echo add_query_arg(['action' => 'edit' ,'id' => $user -> ID]); ?>">
                        <span class="dashicons dashicons-edit-large"></span></a>

                        <a title="حذف کاربر" href="<?php echo add_query_arg(['action' => 'removeUser' ,'id' => $user -> ID]); ?>">
                        <span class="dashicons dashicons-trash"></span></a>

                    </td>
                    
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>

</div>