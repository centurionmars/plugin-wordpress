<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
  display: none;
}
.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}
.slider {
  position: absolute;
  cursor: pointer;
  top: 62%;
  left: 0;
  right: 14%;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
  border: 1px solid black;
  height: 31px;
  width: 62px;
}
.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}
input:checked + .slider {
  background-color: #2196F3;
  border: 1px solid black;
  height: 31px;
  width: 62px;
}
input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}
input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}
/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}
.slider.round:before {
  border-radius: 50%;
}
.active_plugin {
    font-size: 20px;
}
.button-primary {
    margin: 21px 0 0 0;
}
</style>
</head>
<p style="
    color:red;
    color: #680ec0;
    font-size: 41px;
    margin: 1% 30%;
    border: 0px solid black;
    border-radius: 11px;
    padding: 38px;
    box-sizing: border-box;
    text-align: center;
    background-image: linear-gradient(to right bottom, #3d5f929c, #406a96c7, #539aa0, #5da050a6, #a8eb1233);
    ">wp apis plugin</p>
<div class="wrap">
    <h1>تنظیمات پلاگین</h1>
    <form action="" method="post">
        <label for="is_plugin_active" class="active_plugin">
            <input name="is_plugin_active" type="checkbox" id="is_plugin_active" class="switch" 
            style="display: none;"
            <?php echo isset($current_plugin_status) && ($current_plugin_status) > 0 ? 'checked':''; ?>
            >
            <span class="slider round"></span>
            فعال بودن پلاگین : 
        </label>
        <div>
            <button class="button button-primary" type="submit" name="saveSettings" style="margin: 10px 0 0 0;">ذخیره سازی</button>
        </div>
    </form>
</div>

